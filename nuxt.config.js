
export default {
  head: {
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
    ],
  },
  srcDir: 'src/client',
  css: [
    '~/assets/main.scss',
  ],
  build: {
    vendor: [
      'axios',
      './src/shared/validation',
    ],
  },
};
