
module.exports = {
  webpack: (config) => {
    const conf = config;
    conf.entry.main = './src/server/index.js';
    return conf;
  },
};
