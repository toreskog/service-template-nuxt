/* eslint-disable no-param-reassign,no-shadow */
import axios from 'axios';

const state = () => ({
  user: null,
});

const mutations = {
  SET_USER: (state, user) => {
    state.user = user;
  },
};

const actions = {
  nuxtServerInit({ commit }, { req }) {
    if (req.session && req.session.user) {
      commit('SET_USER', req.session.user);
    }
  },
  async signin({ commit }, { email, password }) {
    try {
      const res = await axios.post('/api/signin', { email, password });
      const { user } = res.data;
      commit('SET_USER', user);
    } catch (err) {
      throw err.response.data;
    }
  },
  async signup({ commit }, { email, password }) {
    try {
      const res = await axios.post('/api/signup', { email, password });
      const { user } = res.data;
      commit('SET_USER', user);
    } catch (err) {
      throw err.response.data;
    }
  },
  async signout({ commit }) {
    await axios.get('/api/signout');
    commit('SET_USER', null);
  },
};

export { state, mutations, actions };
