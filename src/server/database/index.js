
import mongoose from 'mongoose';
import logger from '../logger';

class Database {
  static async init() {
    logger.info('Connecting to database');
    try {
      await mongoose.connect('mongodb://mongo/whats-up-next');
    } catch (err) {
      logger.error('Failed to connect to database\n', err);
      process.exit('Failed to connect to database');
    }
  }
}

export default Database;
