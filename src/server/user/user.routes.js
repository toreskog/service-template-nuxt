
import { Router } from 'express';
import { UserService } from './';

const userRoutes = Router();

userRoutes.get('/users', async (req, res) => {
  try {
    return res.status(200).json({
      users: await UserService.findAll(),
    });
  } catch (err) {
    return res.status(400).json({
      errors: err.message,
    });
  }
});

userRoutes.post('/users', async (req, res) => {
  const user = req.body;

  if (!user) {
    return res.status(400).json({
      error: 'No user',
    });
  }

  try {
    return res.status(201).json({
      user: await UserService.create(user),
    });
  } catch (err) {
    return res.status(400).json({
      errors: err.message,
    });
  }
});

export default userRoutes;
