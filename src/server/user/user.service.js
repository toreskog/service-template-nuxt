
import { UserModel } from './';

class UserService {
  static async findById(id) {
    return UserModel.findOne({ _id: id });
  }

  static async findByEmail(email) {
    return UserModel.findOne({ email });
  }

  static async findAll() {
    return UserModel.find({});
  }

  static async create(user) {
    return UserModel.create(user);
  }
}

export default UserService;
