import logger from 'winston';
import bcrypt from 'bcryptjs';
import { Router } from 'express';
import { UserService } from '../user';
import ServerError, { serverErrors } from '../error';
import validate, { signupConstraints } from '../../shared/validation';

const authRoutes = Router();

authRoutes.post('/signin', async (req, res, next) => {
  let user;
  const { email, password } = req.body;

  if (!email || !password) {
    return next(new ServerError(serverErrors.emailAndPasswordIsRequired));
  }

  // Search for user in database
  try {
    user = await UserService.findByEmail(email);
  } catch (err) {
    logger.error('Failed to sign in', err);
    return next(new ServerError({ message: err.message }));
  }

  if (!user) {
    return next(new ServerError(serverErrors.emailNotFound));
  }

  let passwordIsValid;
  try {
    passwordIsValid = await bcrypt.compare(password, user.password);
  } catch (err) {
    logger.error('Failed to sign in', err);
    return next(new ServerError({ message: err.message }));
  }

  if (!passwordIsValid) {
    return next(new ServerError(serverErrors.wrongPassword));
  }

  req.session.user = { id: user.id };
  return res.status(200).json({
    user,
    message: 'Successfully signed in!',
  });
});

authRoutes.post('/signup', async (req, res, next) => {
  const { email, password } = req.body;

  if (!email || !password) {
    return next(new ServerError(serverErrors.emailAndPasswordIsRequired));
  }

  const errors = validate({ email, password }, signupConstraints);
  if (errors) {
    return next(new ServerError({
      ...serverErrors.notValidCredentials,
      errors,
    }));
  }

  // Check if user already exists
  try {
    const user = await UserService.findByEmail(email);
    if (user) {
      return next(new ServerError(serverErrors.emailAlreadyExists));
    }
  } catch (err) {
    logger.error('Failed to sign up\n', err);
    return next(new ServerError({ message: err.message }));
  }


  // Hash password
  let hash;
  try {
    hash = await bcrypt.hash(password, 12);
  } catch (err) {
    logger.error('Failed to sign up\n', err);
    return next(new ServerError({ message: err.message }));
  }

  // Create user and insert to database
  let user = {
    email,
    password: hash,
  };
  try {
    user = await UserService.create(user);
  } catch (err) {
    logger.error('Failed to sign up\n', err);
    return next(new ServerError({ message: err.message }));
  }

  req.session.user = { id: user.id };
  return res.status(201).json({
    status: 201,
    message: 'Successfully signed up',
    user,
  });
});

authRoutes.get('/signout', async (req, res) => {
  await req.session.destroy();
  return res.status(200).json({
    status: 200,
    message: 'Successfully signed out!',
  });
});

export default authRoutes;
