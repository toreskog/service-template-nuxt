
import authRoutes from './auth.routes';
import AuthMiddleware from './auth.middelware';

class Auth {
  /**
   * Init authentication
   */
  static init(app) {
    app.use('/api', authRoutes);
  }
}

export { AuthMiddleware };
export default Auth;
