import winston from 'winston';

const logger = new winston.Logger({
  level: process.env.LOG_LEVEL,
  transports: [
    new (winston.transports.Console)(),
  ],
});

export default logger;
