import { serverErrors } from '../../shared/messages';

class ServerError extends Error {
  constructor({
    message, code, status, errors,
  }) {
    super(message);
    Error.captureStackTrace(this, this.constructor);
    this.code = code || 'serverError';
    this.status = status || 500;
    this.errors = errors;
  }
}

export { serverErrors };
export default ServerError;
