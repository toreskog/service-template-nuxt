

const serverErrors = {
  emailNotFound: {
    message: 'Email not found',
    status: 401,
  },
  wrongPassword: {
    message: 'Wrong password',
    status: 401,
  },
  emailAlreadyExists: {
    message: 'Email does already exist',
    status: 409,
  },
  emailAndPasswordIsRequired: {
    message: 'Email and password is required',
    status: 422,
  },
  notValidCredentials: {
    message: 'Not valid credentials',
    status: 422,
  },
};

Object.keys(serverErrors).forEach((key) => {
  serverErrors[key].code = key;
});

const errors = {
  serverErrors,
};

export { serverErrors };
export default errors;
