const { resolve } = require('path');

module.exports = {
  root: true,
  parserOptions: {
    parser: 'babel-eslint',
  },
  plugins: [
    'vue'
  ],
  extends: [
    'airbnb-base',
    'plugin:vue/recommended',
  ],
  rules: {
    'object-shorthand': ['off'],
  },
  env: {
    node: true,
    browser: true,
  },
  settings: {
    'import/resolver': {
      webpack: {
        config: {
          resolve: {
            alias: {
              '~': resolve(__dirname, 'src', 'client'),
              '@': resolve(__dirname, 'src', 'client')
            }
          }
        }
      }
    }
  }
}
